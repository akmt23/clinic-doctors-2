import getAdminLayout from "components/layouts/adminLayout";
import React, { useEffect } from "react";
import { ElevatedContainer } from "components/atoms/ElevatedContainer";
import { PatientEntity } from "@core/types/patient/IPatient";
import Link from "next/link";
import { useRecoilState } from "recoil";
import { patientsState } from "@recoil/atoms/patients";
import gql from "graphql-tag";
import client from "src/apollo/apollo-client";

const PatientsPage: React.FC<{ patients: PatientEntity[] }> = ({
    patients,
}) => {
    return (
        <div className="">
            <h1 className="text-4xl font-bold mb-4">Пациенты</h1>
            <div className="form-control lg:w-1/2 mb-4">
                <input
                    type="text"
                    placeholder="Поиск"
                    className="input bg-base-200"
                />
            </div>
            <div className="w-full border-b-2 border-base-200 mb-4"></div>
            <div className="grid grid-cols-3 gap-4">
                {patients.map((patient) => (
                    <PatientCard
                        key={patient?._id}
                        patient={patient}
                    ></PatientCard>
                ))}
            </div>
        </div>
    );
};

const PatientCard: React.FC<{ patient: PatientEntity }> = ({ patient }) => {
    return (
        <Link href={`/patients/${patient?._id}/home`}>
            <ElevatedContainer className="rounded-lg p-4 h-full">
                <div className="flex items-center ">
                    <div className="avatar mr-4">
                        <div className="rounded-full w-20 h-20 bg-primary">
                            <span className="text-4xl text-white uppercase flex items-center justify-center h-full">
                                {patient.fullName[0]}
                            </span>
                        </div>
                    </div>
                    <div className="flex flex-col justify-start">
                        <span className="text-2xl font-medium w-1/2 h-16">
                            {patient.fullName}
                        </span>
                        <span className="text-base-300">ИИН: 0123123212</span>
                    </div>
                </div>
            </ElevatedContainer>
        </Link>
    );
};

export async function getStaticProps() {
    const PATIENTS_LIST = gql`
        query {
            listUsers {
                fullName
                _id
                photoURL {
                    m
                }
            }
        }
    `;
    const { data } = await client.query({
        query: PATIENTS_LIST,
    });
    const patients = data?.listUsers;

    return {
        props: { patients },
        revalidate: 10,
        // will be passed to the page component as props
    };
}
export default PatientsPage;
